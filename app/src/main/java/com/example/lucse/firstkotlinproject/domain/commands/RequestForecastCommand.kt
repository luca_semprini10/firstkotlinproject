package com.example.lucse.firstkotlinproject.domain.commands

import com.example.lucse.firstkotlinproject.domain.datasource.ForecastProvider
import com.example.lucse.firstkotlinproject.domain.model.ForecastList

class RequestForecastCommand(
        private val zipCode: Long,
        private val forecastProvider: ForecastProvider = ForecastProvider()) :
        Command<ForecastList> {

    companion object {
        val DAYS = 7
    }

    override fun execute() = forecastProvider.requestByZipCode(zipCode, DAYS)
}