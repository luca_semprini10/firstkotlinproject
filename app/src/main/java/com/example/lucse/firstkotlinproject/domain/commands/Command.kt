package com.example.lucse.firstkotlinproject.domain.commands

public interface Command<out T> {
    fun execute(): T
}
