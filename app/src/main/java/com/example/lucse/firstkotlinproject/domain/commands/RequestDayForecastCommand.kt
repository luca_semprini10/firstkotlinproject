package com.example.lucse.firstkotlinproject.domain.commands

import com.example.lucse.firstkotlinproject.domain.datasource.ForecastProvider
import com.example.lucse.firstkotlinproject.domain.model.Forecast

class RequestDayForecastCommand(
        val id: Long,
        private val forecastProvider: ForecastProvider = ForecastProvider()) :
        Command<Forecast> {

    override fun execute() = forecastProvider.requestForecast(id)
}
