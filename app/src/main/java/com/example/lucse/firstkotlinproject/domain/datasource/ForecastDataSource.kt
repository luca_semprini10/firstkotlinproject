package com.example.lucse.firstkotlinproject.domain.datasource

import com.example.lucse.firstkotlinproject.domain.model.Forecast
import com.example.lucse.firstkotlinproject.domain.model.ForecastList

interface ForecastDataSource {

    fun requestForecastByZipCode(zipCode: Long, date: Long): ForecastList?

    fun requestDayForecast(id: Long): Forecast?

}
