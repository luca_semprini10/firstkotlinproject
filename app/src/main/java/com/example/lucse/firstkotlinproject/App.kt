package com.example.lucse.firstkotlinproject

import android.app.Application
import com.example.lucse.firstkotlinproject.extensions.DelegatesExt

class App : Application() {

    companion object {
        var instance: App by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
